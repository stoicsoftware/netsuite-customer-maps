## NetSuite Customer Maps

Adds some simple mapping capabilities for Customer addresses in NetSuite.

*Features*:

* Display a map on the Customer record showing all listed Addresses
* Show map of all primary Customer addresses based on Saved Search results
