define([], function () {

    /**
     * Adds and populates the map on the Customer record
     *
     * @exports maps/cu-ue
     *
     * @copyright 2017 Stoic Software, LLC
     * @author Eric T Grubaugh <eric@stoic.software>
     *
     * @NApiVersion 2.x
     * @NModuleScope SameAccount
     * @NScriptType UserEventScript
     * @appliedtorecord customer
     */
    var exports = {};

    /**
     * <code>beforeLoad</code> event handler
     * for details.
     *
     * @governance XXX
     *
     * @param context
     *        {Object}
     * @param context.newRecord
     *        {record} The new record being loaded
     * @param context.type
     *        {UserEventType} The action type that triggered this event
     * @param context.form
     *        {form} The current UI form
     *
     * @return {void}
     *
     * @static
     * @function beforeLoad
     */
    function beforeLoad(context) {
        // TODO
    }


    exports.beforeLoad = beforeLoad;
    return exports;
});
